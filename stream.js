const promisify = require("util").promisify
const stream = require("stream")

module.exports.finished = promisify(stream.finished)
module.exports.pipeline = promisify(stream.pipeline)

import {
  RSAKeyPairOptions,
  RSAKeyPairKeyObjectOptions,
  KeyPairKeyObjectResult,
  DSAKeyPairOptions,
  DSAKeyPairKeyObjectOptions,
  ECKeyPairOptions,
  ECKeyPairKeyObjectOptions,
  BinaryLike,
  Binary,
  ScryptOptions
} from "crypto"

export function generateKeyPair(
  type: "rsa",
  options: RSAKeyPairOptions<"pem", "pem">
): Promise<{ publicKey: string; privateKey: string }>
export function generateKeyPair(
  type: "rsa",
  options: RSAKeyPairOptions<"pem", "der">
): Promise<{ publicKey: string; privateKey: Buffer }>
export function generateKeyPair(
  type: "rsa",
  options: RSAKeyPairOptions<"der", "pem">
): Promise<{ publicKey: Buffer; privateKey: string }>
export function generateKeyPair(
  type: "rsa",
  options: RSAKeyPairOptions<"der", "der">
): Promise<{ publicKey: Buffer; privateKey: Buffer }>
export function generateKeyPair(
  type: "rsa",
  options: RSAKeyPairKeyObjectOptions
): Promise<KeyPairKeyObjectResult>

export function generateKeyPair(
  type: "dsa",
  options: DSAKeyPairOptions<"pem", "pem">
): Promise<{ publicKey: string; privateKey: string }>
export function generateKeyPair(
  type: "dsa",
  options: DSAKeyPairOptions<"pem", "der">
): Promise<{ publicKey: string; privateKey: Buffer }>
export function generateKeyPair(
  type: "dsa",
  options: DSAKeyPairOptions<"der", "pem">
): Promise<{ publicKey: Buffer; privateKey: string }>
export function generateKeyPair(
  type: "dsa",
  options: DSAKeyPairOptions<"der", "der">
): Promise<{ publicKey: Buffer; privateKey: Buffer }>
export function generateKeyPair(
  type: "dsa",
  options: DSAKeyPairKeyObjectOptions
): Promise<KeyPairKeyObjectResult>

export function generateKeyPair(
  type: "ec",
  options: ECKeyPairOptions<"pem", "pem">
): Promise<{ publicKey: string; privateKey: string }>
export function generateKeyPair(
  type: "ec",
  options: ECKeyPairOptions<"pem", "der">
): Promise<{ publicKey: string; privateKey: Buffer }>
export function generateKeyPair(
  type: "ec",
  options: ECKeyPairOptions<"der", "pem">
): Promise<{ publicKey: Buffer; privateKey: string }>
export function generateKeyPair(
  type: "ec",
  options: ECKeyPairOptions<"der", "der">
): Promise<{ publicKey: Buffer; privateKey: Buffer }>
export function generateKeyPair(
  type: "ec",
  options: ECKeyPairKeyObjectOptions
): Promise<KeyPairKeyObjectResult>

export function pbkdf2(
  password: BinaryLike,
  salt: BinaryLike,
  iterations: number,
  keylen: number,
  digest: string
): Promise<Buffer>

export function randomBytes(size: number): Promise<Buffer>

export function randomFill<T extends Binary>(buffer: T): Promise<T>
export function randomFill<T extends Binary>(
  buffer: T,
  offset: number
): Promise<T>
export function randomFill<T extends Binary>(
  buffer: T,
  offset: number,
  size: number
): Promise<T>

export function scrypt(
  password: BinaryLike,
  salt: BinaryLike,
  keylen: number
): Promise<Buffer>
export function scrypt(
  password: BinaryLike,
  salt: BinaryLike,
  keylen: number,
  options: ScryptOptions
): Promise<Buffer>

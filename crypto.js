const promisify = require("util").promisify
const crypto = require("crypto")

module.exports.generateKeyPair = promisify(crypto.generateKeyPair)
module.exports.pbkdf2 = promisify(crypto.pbkdf2)
module.exports.randomBytes = promisify(crypto.randomBytes)
module.exports.randomFill = promisify(crypto.randomFill)
module.exports.scrypt = promisify(crypto.scrypt)

# @improved/node

Quickly import from core node modules

## Usage

(TypeScript)

```typescript
import { ok } from "@improved/node/assert" // assert strict mode
import { readFile } from "@improved/node/fs" // fs Promises API
import { join as posixJoin } from "@improved/node/path-posix" // path.posix
import { join as win32Join } from "@improved/node/path-win32" // path.win32
import { decode } from "@improved/node/punycode-ucs2" // punycode.ucs2
```

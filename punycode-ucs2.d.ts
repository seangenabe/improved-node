export function decode(string: string): number[]
export function encode(codePoints: number[]): string

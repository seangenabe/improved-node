export function finished(
  stream: NodeJS.ReadableStream | NodeJS.WritableStream
): Promise<void>
export function pipeline(
  stream1: NodeJS.ReadableStream,
  stream2: NodeJS.WritableStream
): Promise<void>
export function pipeline(
  stream1: NodeJS.ReadableStream,
  stream2: NodeJS.ReadWriteStream,
  stream3: NodeJS.WritableStream
): Promise<void>
export function pipeline(
  stream1: NodeJS.ReadableStream,
  stream2: NodeJS.ReadWriteStream,
  stream3: NodeJS.ReadWriteStream,
  stream4: NodeJS.WritableStream
): Promise<void>
export function pipeline(
  stream1: NodeJS.ReadableStream,
  stream2: NodeJS.ReadWriteStream,
  stream3: NodeJS.ReadWriteStream,
  stream4: NodeJS.ReadWriteStream,
  stream5: NodeJS.WritableStream
): Promise<void>
export function pipeline(
  streams: Array<
    NodeJS.ReadableStream | NodeJS.WritableStream | NodeJS.ReadWriteStream
  >
): Promise<void>
export function pipeline(
  stream1: NodeJS.ReadableStream,
  stream2: NodeJS.ReadWriteStream | NodeJS.WritableStream,
  ...streams: Array<NodeJS.ReadWriteStream | NodeJS.WritableStream>
): Promise<void>

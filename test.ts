import { test, run } from "t0"
import { strictEqual as equal, ok } from "assert"
import { equal as strictEqual1 } from "./assert"
const strictEqual2 = require("assert").strict.equal
import { readFile } from "./fs"
const readFilePromise = require("fs").promises.readFile
import { join as posixJoin1 } from "./path-posix"
const posixJoin2 = require("path").posix.join
import { join as win32Join1 } from "./path-win32"
const win32Join2 = require("path").win32.join
import { decode as decode1 } from "./punycode-ucs2"
const decode2 = require("punycode").ucs2.decode
import { hrtime } from "./process"
import { randomBytes } from "./crypto"

test("assert", () => {
  equal(strictEqual1, strictEqual2)
})

test("fs", () => {
  equal(readFile, readFilePromise)
})

test("path", () => {
  equal(posixJoin1, posixJoin2)
  equal(win32Join1, win32Join2)
})

test("punycode", () => {
  equal(decode1, decode2)
})

test("process", () => {
  ok(typeof hrtime() === "bigint")
})

test("crypto", async () => {
  const bytes = randomBytes(8)
  ok(bytes instanceof Promise)
  ok((await bytes) instanceof Buffer)
})

run()
